# UMN Proxy Server

The UMN Proxy plugs into the synchronization mechanism and transparently relies synchronization commands
over a http(s) web socket protocol to a remote
UMN Proxy Server. Typically you employ the UMN Proxy setup when:

* You don't want to expose your database directly over the internet, but you want it to be hidden
  behind a firewall
* You are developing an Android app.
* You don't want to go through the hassle of setting up MongoDB/Hazelcast/Kafka server.

Say that you sync over a MongoDB server without the UMN Proxy. Then the communication path of your app
looks like the following:

```
[Your App] --uses--> [UMN] <---uses---> [SyncMongoDBClient] <---INTERNET---> [MongoDB]
```

The problem is that the `SyncMongoDBClient` class requires MongoDB Java client which doesn't work on Android;
also your database needs to be exposed over the internet. You can solve both of these issues
by using the UMN Proxy. The Proxy relies the synchronization commands using a very thin [umn-proxy-client](../umn-proxy-client)
client which uses http(s) websockets:

```
[Your App] --uses--> [UMN] <---uses---> [ProxyClient] <---INTERNET---> [UMN Proxy Server] --uses--> [SyncMongoDBClient] --localhost--> [MongoDB]
```

UMN Proxy supports two modes of operation:

* The Proxy Mode
* The Standalone Mode

## The Proxy Mode

In this mode, the UMN Proxy only relies the synchronization commands to the actual database.
Therefore, you need to run both the UMN Proxy Server and the actual database server (such as MongoDB, Hazelcast or
Kafka) on your server machine. If you wish your setup to be simpler, use the Standalone Mode.

## The Standalone Mode

UMN Proxy Server is capable of not only acting as a proxy (relying sync commands to, say, a MongoDB server) -
it can be configured to also handle the duty of storing of the log (simply by using the `MVStoreGlobalLog`
to store it into a H2 MVStore database).

Therefore, if you are shooting for the most simple setup, you don't need Kafka nor MongoDB nor Hazelcast -
all you need is a UMN Proxy server up and running. Since UMN Proxy only requires Java JRE to run,
it's extremely easy to launch and extremely portable.

See below on how to configure UMN Proxy Server in Standalone Mode.

## More technical explanation

UMN uses the [GlobalLogClient](../umn-core/src/main/kotlin/com/gitlab/mvysny/umn/sync/GlobalLogClient.kt)
interface to access a common server to perform the synchronization with all other UMN clients.
Every server then provides its own implementation of the `GlobalLogClient`,
which is just a thin wrapper wrapping native client for that particular server.
For example, the [UMN MongoDB Sync](../umn-sync-mongodb)
provides the `SyncMongoDBClient` class which implements the `GlobalLogClient` interface and uses
the [MongoDB Java client](https://mongodb.github.io/mongo-java-driver/) to push log items to MongoDB queues. The very same idea is used by the Kafka Sync,
Hazelcast sync and others.

The problem is that the [MongoDB Java Client doesn't work on Android](https://gitlab.com/mvysny/umn/issues/1). And neither
does Hazelcast nor Kafka client. They only work on a full-blown JRE. So you do exactly that -
you run the MongoDB client on your Java server along with your database, and use UMN Proxy to relay
the synchronization commands to your server.

## Configuration and Deployment

The deployment is simple. First you need to compile the server:

1. Install Java JDK 8 or higher
2. `git clone https://gitlab.com/mvysny/umn`
3. `cd umn && ./gradlew umn-proxy-server:build -x test`
4. Find the zip file at `umn-proxy-server/build/distributions/*.zip`

To deploy:

1. Install Java JRE 8 or higher
2. Unzip the zip file
3. Run `./proxy`

The server will load the configuration file from the following locations:

1. `config.yaml` at the current working directory
2. `/etc/umnproxyserver/config.yaml`

Example content of the configuration file:

```yaml
port: 10144
host: 0.0.0.0
adminPort: 10145
adminHost: 127.0.0.1
proxy:
  standalone:
    db: ./db
secure:
  port: 10146
  host: 0.0.0.0
  pkcs12: /opt/keystore.p12
```

Currently only the standalone mode is supported. The database will be created automatically. To
create users, please use the Admin REST API as described below.

### Standalone Mode

In this mode the UMN Proxy stores the record log using the embedded H2 MVStorage database, and therefore
it doesn't require Kafka/MongoDB/Hazelcast server to store the record log.

The only configuration is the `db` which is a directory where the database file (just a single file) will
be located. See the `config.yaml` example above for more details.

### Secure WebSockets over SSL/HTTPS

The UMN Proxy Server supports secure WebSockets over HTTPS/SSL. In order to activate this, you'll need
your certificate and private key stored in a passwordless PKCS12 key store. Then, just
make sure to provide the `secure:` stanza in the `config.yaml` as shown above.

In order to use the secure sockets, you need to provide the `wss://somehost:10146` URL to your UMN Proxy client.

#### Let's Encrypt

You can use UMN Proxy server with Let's Encrypt easily. You just need to make sure to
convert PEM produced by Let's Encrypt to PKCS12 (taken from excellent [LetsEncrypt certs with embedded Jetty on](https://danielflower.github.io/2017/04/08/Lets-Encrypt-Certs-with-embedded-Jetty.html)):

```bash
cd /etc/letsencrypt/live/www.crickam.com
openssl pkcs12 -export -in fullchain.pem -inkey privkey.pem -out /opt/keystore.p12 -name crickam -CAfile chain.pem -caname root -passout "pass:"
chmod a+r /opt/keystore.p12
```

Then configure UMN to activate the secure socket on port 10146: add the following to your `config.yaml`:
```yaml
secure:
  port: 10146
  host: 0.0.0.0
  pkcs12: /opt/keystore.p12
```

Don't forget to add the post hook to convert the keystore after it has been updated by Certbot, and
to restart the UMN Proxy Server to load the new key.

## Admin Interface

UMN Proxy Server offers the administration interface as a REST endpoint. By default the interface
listens on http 10145 on localhost only, no credentials are required.

To control the UMN Proxy Server programmatically, see the Java client: [umn-proxy-server-adminclient](../umn-proxy-server-adminclient).

### User management

The user management only works when the UMN Proxy Server is running in the Standalone mode.
The purpose of this API is to add, update and remove users that are allowed to sync via the UMN Proxy Client.
For every user a separate global log is created.

> *Note:* When the UMN Proxy Server is not running in standalone mode, the log is stored in a separate database such as MongoDB.
Use the database built-in mechanisms to manage users.

| Method+Endpoint | Explanation
| --------------- | -----------------------------
| GET /admin/users | Returns all users, as a JSON List of String usernames. Example: `curl -i localhost:10145/admin/users` should print `["user", "user25"]`
| PUT /admin/users | Creates a new user with given plaintext password. Requires two parameters, passed as query params: `username` and `password`. If the user already exists, its password is updated. Example: `curl -i -X PUT "localhost:10145/admin/users?username=user&password=example"` should print 204 NO CONTENT on success.
| DELETE /admin/users/:username | Deletes the user with given username. Example: `curl -i -X DELETE localhost:10145/admin/users/user` should print 204 NO CONTENT on success.

### Statistics

| Method+Endpoint | Explanation
| --------------- | -----------------------------
| GET /admin/stats/logsizes | Returns all users and their log sizes, e.g. `curl -i localhost:10145/admin/stats/logsizes` should print `{"user":25, "user2": 1645}`

## Using in docker

Please see [UMN Proxy Server Docker Documentation](src/main/docker) for more info.
