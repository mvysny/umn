# UMN Kafka Client

Uses Kafka jar to connect to [Apache Kafka](https://kafka.apache.org/) server and synchronize using a shared Kafka log.

## Tutorial

TBD

## Security

TBD.

## Issues:

* Kafka Client does not work on Android: https://stackoverflow.com/questions/40043532/how-to-use-android-app-as-a-producing-client-for-kafka

