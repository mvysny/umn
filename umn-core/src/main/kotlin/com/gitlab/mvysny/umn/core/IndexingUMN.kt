package com.gitlab.mvysny.umn.core

import com.github.mvysny.vokdataloader.DataLoader
import com.github.mvysny.vokdataloader.Filter
import com.github.mvysny.vokdataloader.SortClause
import com.github.mvysny.vokdataloader.intRange
import com.gitlab.mvysny.umn.core.IndexingUMN.IndexDef
import com.gitlab.mvysny.umn.core.IndexingUMN.IndexDef.Companion.simple
import org.h2.mvstore.type.DataType
import java.io.Serializable
import java.util.*
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock
import kotlin.reflect.KClass

/**
 * Generates indices for documents. The purpose of this class is multiple:
 * * To look up multiple documents stored under the same key very quickly
 * * To have an UNIQUE constraint
 * * The keys are [Comparable] - it must be possible to quickly select a list of documents from given key range, sorted by given key;
 * the list must be pageable.
 *
 * Custom key classes should be supported, to allow quick lookup. If that is not possible, at least a [List] of basic [Comparable]s must be
 * supported. For example you can have a key with two parts: the `key` itself which is used to range-select the values, and the `ordering`
 * part which is then used to order the paged results.
 *
 * The indices are never re-generated on function change; it is imperative that the [IndexDef.keyGetter] function is stable and always returns
 * the same set for given document. It is currently not possible to update an index for all documents; any changes to the [IndexDef.keyGetter]
 * function will be applied to newly inserted or updated documents only. You can not currently create a new index over a database with
 * existing documents.
 *
 * ### Implementation
 * Intercepts all calls and passes them to underlying map, but updates the indices on the fly.
 *
 * Optimized for unique indices or indices with low number of collisions. If the index is expected to have high number of
 * collisions, use TODO.
 *
 * @property delegate delegate all calls to the underlying UMN storage. Typically either one of [InMemoryUMN] or [MvstoreUMN].
 * @param indices the indices definition. WARNING: the same set of indices must be provided upon every UMN open! New indices not yet present
 * in the database will not reindex existing documents to build the index and searches will not find those documents. Changing the indexing
 * function will not reindex existing documents. Removing an index will not clear the associated index map.
 */
public class IndexingUMN(override val delegate: UMN, indices: Set<IndexDef<*, *, *>>) : UMN {

    /**
     * Maps index name to the index implementor.
     */
    private val indexMap: Map<String, IndexImpl<*, *, *>> = indices.associate { it.name to it.toImpl(this) }

    override fun create(document: Any, id: UUID): Boolean {
        return indexUpdaterGlobalLock.withLock {
            val created = delegate.create(document, id)
            if (created) {
                indexMap.values.forEach { it.onCreated(id, document) }
            }
            created
        }
    }

    override fun create(document: Any): UUID {
        return indexUpdaterGlobalLock.withLock {
            val id = delegate.create(document)
            indexMap.values.forEach { it.onCreated(id, document) }
            id
        }
    }

    /**
     * An index definition. Index provides very quick lookup of a document for given key.
     *
     * Typically, index retrieves keys for a document and then maintains the mapping of a key to its document. That's the simple use-case,
     * simply use [simple] to produce indices of this type. That is the most common usage.
     *
     * However, sometimes you don't want to map keys to the document being indexed, but to something else, say to some kind of its parent.
     * This would save you the time which would be otherwise wasted by loading of the document, retrieving its parent ID from the document,
     * and loading the parent.
     *
     * WARNING: if the key is not directly supported by [UtilityMap] then it might be serialized into the underlying storage by using traditional
     * Java serialization procedure. Extreme care must be taken so that the serialization form doesn't change over time. It's strongly
     * recommended for [K] to implement the [java.io.Externalizable] interface.
     *
     * @property K the type of keys. Consult the underlying storage for [UtilityMap] key types. Yet, at least [String] and [Long] must always
     * be supported. The type must be comparable and must properly implement [Any.equals] and [Any.hashCode]. Often the key is just a String,
     * but sometimes it's more complex comparable so that we can select a range of keys and have the values sorted.
     * @property D the type of documents indexed by this index. The indexing function is run on these documents as they are created or updated
     * within the [UMN].
     * @property V the type of values associated with the keys. Typically it's the same as [D]. It's the type of documents for which the
     * [valueIdGetter] provides IDs for.
     * @property name the unique name of the index
     * @property keyGetter computes keys for given document. If no indices are required for this document or if the document is not indexed
     * by this index, just return an empty set.
     * The function must run quick, must be pure and must be consistent in computing the same set of keys for a particular document.
     * @property documentType the type of documents indexed by this index. The [keyGetter] will only be called for documents of this type -
     * all other document types will be ignored and will not be indexed by this index.
     * @property valueType the type of documents associated with the keys. Typically equal to [documentType] for simple indices.
     * @property valueIdGetter which ID to actually associate with the key. Receives the document ID and the document itself. By default simply
     * returns the ID
     */
    public class IndexDef<K, D : Any, V : Any>(
            public val name: String,
            public val keyGetter: (D) -> Set<K>,
            public val documentType: KClass<D>,
            public val valueType: KClass<V>,
            public val valueIdGetter: (UUID, D) -> UUID
    ) where K: Comparable<K>, K: Serializable {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false
            other as IndexDef<*, *, *>
            if (name != other.name) return false
            return true
        }

        override fun hashCode(): Int = name.hashCode()
        override fun toString(): String = "IndexDef(name='$name')"

        internal fun toImpl(umn: IndexingUMN): IndexImpl<K, D, V> = umn.IndexImpl(this)

        public companion object {
            /**
             * Creates a simple index which simply maps keys to the ID of the document.
             * @param name the unique name of the index
             * @param keyGetter computes keys for given document. If no indices are required for this document or if the document is not indexed
             * by this index, just return an empty set.
             * The function must run quick, must be pure and must be consistent in computing the same set of keys for a particular document.
             * @property documentType the type of documents indexed by this index. The [keyGetter] will only be called for documents of this type -
             * all other document types will be ignored and will not be indexed by this index.
             */
            public fun <K, D: Any> simple(name: String, keyGetter: (D) -> Set<K>, documentType: KClass<D>) : IndexDef<K, D, D> where K: Comparable<K>, K:Serializable =
                    IndexDef(name, keyGetter, documentType, documentType, { id, _ -> id })
        }
    }

    /**
     * Prevents multiple threads from updating indices of a particular document at the same time.
     *
     * Pozor! Nestaci zamknut lock iba v [IndexImpl.registerUnregisterIndices]: ak naraz updatuje jeden dokument viacero threadov,
     * operacie zapisu mozu bezat v inom poradi ako update indexov! Preto treba zamknut komplet cele metody.
     */
    private val indexUpdaterGlobalLock = ReentrantLock()

    /**
     * @property K the type of keys (objects acting as index)
     * @property D the type of documents indexed by this index.
     * @property V the type of documents referenced by the keys.
     */
    public inner class IndexImpl<K, D: Any, V: Any>(public val def: IndexDef<K, D, V>) where K: Comparable<K>, K: Serializable {

        /**
         * Maps key to a set of documents IDs (the [V] documents). Don't use Set since that's serialized using Java serialization; use Arrays
         * since that's serialized more effectively; see [ObjectDataType#TYPE_ARRAY] for more info.
         */
        private val indexStore: UtilityMap<K, Array<UUID>> = delegate.openUtilityMap("INDEX2_${def.name}")

        private fun registerUnregisterIndices(id: UUID, register: Set<K>, unregister: Set<K>) {
            check(indexUpdaterGlobalLock.isHeldByCurrentThread) { "lock not held!" }
            val indicesToRegister: Set<K> = register - unregister
            val indicesToUnregister: Set<K> = unregister - register
            indicesToRegister.forEach { index ->
                var array: Array<UUID>? = indexStore.find(index)
                if (array == null) {
                    array = arrayOf(id)
                    indexStore.put(index, array)
                } else if (!array.contains(id)) {
                    array += id
                    indexStore.put(index, array)
                }
            }
            indicesToUnregister.forEach { index ->
                val array = indexStore.find(index)
                if (array != null && array.contains(id)) {
                    val newArray = array.filterNot { it == id }.toTypedArray()
                    indexStore.put(index, if (newArray.isEmpty()) null else newArray)
                }
            }
        }

        /**
         * Finds document matching lowest key. If there are multiple documents matching the lowest key, one is picked arbitrarily.
         * @param forKeys only consider documents mapped to these keys.
         */
        public fun findFirstDoc(forKeys: For<K>): Pair<UUID, V>? = getSequenceOfDocs(forKeys).firstOrNull()

        /**
         * Finds document ID matching lowest key. If there are multiple documents matching the lowest key, one is picked arbitrarily.
         * @param forKeys only consider documents mapped to these keys.
         */
        public fun findFirstID(forKeys: For<K>): UUID? = getSequenceOfIDs(forKeys).firstOrNull()

        /**
         * Finds all document IDs matching given keys. Returns an ordered set of IDs; documents with lower keys appearing sooner in the set.
         * @param forKeys only consider documents mapped to these keys.
         */
        public fun findAllIDs(forKeys: For<K>): Set<UUID> = getSequenceOfIDs(forKeys).toSet()

        /**
         * Finds all documents matching given keys. Returns an ordered map of IDs mapped to documents;
         * documents with lower keys appearing sooner in the set.
         * @param forKeys only consider documents mapped to these keys.
         */
        public fun findAllDocs(forKeys: For<K>): Map<UUID, V> = getSequenceOfDocs(forKeys).toMap()

        /**
         * Returns all documents matching given keys. The returned map is ordered - the keys are enumerated in increasing order.
         * @param page the offset and the limit. By default retrieves all matching documents.
         * @return a map of documents and their respective IDs. The map is ordered by the matching keys, ascending. May be empty.
         */
        public fun findAllDocsPaged(forKeys: For<K>, page: IntRange = 0..Int.MAX_VALUE): Map<UUID, V> {
            if (page.isEmpty() || forKeys.isEmpty) return mapOf()
            if (page == 0..Int.MAX_VALUE) return findAllDocs(forKeys)

            val result = mutableMapOf<UUID, V>()
            var currentIndex = 0
            getSequenceOfIDs(forKeys).forEach { id ->
                if (!result.containsKey(id) && delegate.exists(id)) {
                    if (currentIndex in page) {
                        val doc = delegate.find(id, def.valueType)
                        if (doc != null) result[id] = doc
                    }
                    currentIndex++
                    if (currentIndex > page.last) return result
                }
            }
            return result
        }

        /**
         * Returns all document IDs matching given keys. The returned map is ordered - the keys are enumerated in increasing order.
         * @param page the offset and the limit. By default retrieves all matching documents.
         * @return a set of document IDs. The set is ordered by the matching keys, ascending. May be empty.
         */
        public fun findAllIDsPaged(forKeys: For<K>, page: IntRange = 0..Int.MAX_VALUE): Set<UUID> {
            if (page.isEmpty() || forKeys.isEmpty) return setOf()
            if (page == 0..Int.MAX_VALUE) return findAllIDs(forKeys)

            val result = mutableSetOf<UUID>()
            var currentIndex = 0
            getSequenceOfIDs(forKeys).forEach { id ->
                if (!result.contains(id)) {
                    if (currentIndex in page) {
                        result.add(id)
                    }
                    currentIndex++
                    if (currentIndex > page.last) return result
                }
            }
            return result
        }

        /**
         * Counts all documents keyed with given keys.
         *
         * May count in non-existing documents if the documents were deleted during the run
         * of this function.
         * @return count of matching documents, 0 or greater.
         */
        public fun getCount(forKeys: For<K>): Int {
            // @todo mavi unique index size can simply be computed as indexStore.size()
            if (forKeys.isEmpty) return 0
            return when (forKeys) {
                is For.Key -> indexStore.find(forKeys.key)?.size ?: 0
                else -> getSequenceOfIDs(forKeys).distinct().count()
            }
        }

        public fun getSize(): Int = getCount(For.all())

        internal fun onCreated(id: UUID, document: Any) {
            val docType = def.documentType.java
            if (!docType.isInstance(document)) return
            val doc = docType.cast(document)
            val valueId = def.valueIdGetter(id, doc)
            registerUnregisterIndices(valueId, def.keyGetter(doc), setOf())
        }

        internal fun onDeleted(id: UUID, document: Any) {
            val docType = def.documentType.java
            if (!docType.isInstance(document)) return
            val doc = docType.cast(document)
            val unregisterIndices: Set<K> = def.keyGetter(doc)
            val valueId = def.valueIdGetter(id, doc)
            registerUnregisterIndices(valueId, setOf(), unregisterIndices)
        }

        internal fun onUpdate(id: UUID, newDoc: Any, oldDoc: Any) {
            val docType = def.documentType.java
            val newDocOfCorrectType = docType.isInstance(newDoc)
            val oldDocOfCorrectType = docType.isInstance(oldDoc)
            if (!newDocOfCorrectType && !oldDocOfCorrectType) {
                // do nothing
            } else if (newDocOfCorrectType && !oldDocOfCorrectType) {
                val doc = docType.cast(newDoc)
                val registerIndices = def.keyGetter(doc)
                val valueId = def.valueIdGetter(id, doc)
                registerUnregisterIndices(valueId, registerIndices, setOf())
            } else if (!newDocOfCorrectType && oldDocOfCorrectType) {
                val doc = docType.cast(oldDoc)
                val unregisterIndices = def.keyGetter(doc)
                val valueId = def.valueIdGetter(id, doc)
                registerUnregisterIndices(valueId, setOf(), unregisterIndices)
            } else {
                val oldDocD = docType.cast(oldDoc)
                val newDocD = docType.cast(newDoc)
                val oldValueId = def.valueIdGetter(id, oldDocD)
                val newValueId = def.valueIdGetter(id, newDocD)
                val unregisterIndices = def.keyGetter(oldDocD)
                val registerIndices = def.keyGetter(newDocD)
                if (oldValueId == newValueId) {
                    registerUnregisterIndices(newValueId, registerIndices, unregisterIndices)
                } else {
                    registerUnregisterIndices(oldValueId, setOf(), unregisterIndices)
                    registerUnregisterIndices(newValueId, registerIndices, setOf())
                }
            }
        }

        /**
         * Checks whether there are no documents in this index.
         */
        public fun isEmpty(): Boolean = indexStore.isEmpty()

        /**
         * Returns a lazily evaluated sequence of document IDs stored under given [forKeys]. The sequence will follow key ordering:
         * it will return documents mapped to lower keys sooner.
         *
         * This function is faster than [getSequenceOfDocs] since it doesn't load the documents.
         *
         * It is safe to modify the index (e.g. deleting documents) while iterating.
         * The sequence must not reflect further modifications; the sequence may thus return documents that were meanwhile deleted.
         *
         * Note: only IDs of indexed documents (documents for which the indexing function returned a non-null non-empty set) are returned.
         *
         * Note: the returned IDs may not be unique and may repeat in the sequence.
         * @param forKeys only consider documents mapped to these keys.
         */
        public fun getSequenceOfIDs(forKeys: For<K>): Sequence<UUID> {
            if (forKeys.isEmpty) return emptySequence()
            return when (forKeys) {
                is For.All -> indexStore.values().asSequence().flatMap { it.asSequence() }
                is For.Key -> indexStore.find(forKeys.key)?.asSequence() ?: emptySequence()
                is For.KeySet -> forKeys.set.sorted().asSequence().mapNotNull { key -> indexStore.find(key) } .flatMap { it.asSequence() }
                is For.KeyRange -> indexStore.keyIterator(forKeys.range).asSequence().mapNotNull { key -> indexStore.find(key) } .flatMap { it.asSequence() }
            }
        }

        /**
         * Returns a lazily evaluated sequence of documents and their IDs stored under given [forKeys]. The sequence will follow key ordering:
         * it will return documents mapped to lower keys sooner.
         *
         * This function is slower than [getSequenceOfIDs] since it doesn't load the documents.
         *
         * It is safe to modify the index (e.g. deleting documents) while iterating.
         * The sequence must not reflect further modifications; however the sequence will only return documents that are still available.
         *
         * Note: only IDs of indexed documents (documents for which the indexing function returned a non-null non-empty set) are returned.
         *
         * Note: the returned IDs may not be unique and may repeat in the sequence.
         * @param forKeys only consider documents mapped to these keys.
         */
        public fun getSequenceOfDocs(forKeys: For<K>): Sequence<Pair<UUID, V>> = getSequenceOfIDs(forKeys).mapNotNull { id ->
            val doc = delegate.find(id, def.valueType)
            if (doc == null) null else id to doc
        }

        /**
         * Finds all keys matching [keys].
         */
        public fun keySequence(keys: For<K>): Sequence<K> {
            if (keys.isEmpty) return emptySequence()
            return when (keys) {
                is For.All -> indexStore.keyIterator()
                is For.Key -> indexStore.keyIterator(keys.key..keys.key)
                is For.KeyRange -> indexStore.keyIterator(keys.range)
                is For.KeySet -> keys.set.asSequence().filter { indexStore.containsKey(it) }
            }
        }

        /**
         * Private since the sequence reflects modifications.
         */
        private fun getDescSequenceOfDocs(forKeys: For<K>): Sequence<Pair<UUID, V>> = getDescSequenceOfIDs(forKeys).mapNotNull { id ->
            val doc = delegate.find(id, def.valueType)
            if (doc == null) null else id to doc
        }

        /**
         * Private since the sequence reflects modifications.
         */
        private fun getDescSequenceOfAllDocIDs(maxKey: K? = indexStore.getLargestKey()): Sequence<UUID> =
            generateSequence(maxKey) { id -> indexStore.lowerKey(id) }
                .mapNotNull { key -> indexStore.find(key) }
                .flatMap { it.asSequence() }

        /**
         * Private since the sequence reflects modifications.
         */
        private fun getDescSequenceOfIDs(forKeys: For<K>): Sequence<UUID> {
            if (forKeys.isEmpty) return emptySequence()
            return when (forKeys) {
                is For.All -> getDescSequenceOfAllDocIDs()
                is For.Key -> indexStore.find(forKeys.key)?.asSequence() ?: emptySequence()
                is For.KeySet -> forKeys.set.sorted().asReversed().asSequence().mapNotNull { key -> indexStore.find(key) } .flatMap { it.asSequence() }
                is For.KeyRange -> getDescSequenceOfAllDocIDs(forKeys.range.endInclusive)
            }
        }

        /**
         * Finds document matching lowest key. If there are multiple documents matching the lowest key, one is picked arbitrarily.
         * @param forKeys only consider documents mapped to these keys.
         */
        public fun findLastDoc(forKeys: For<K>): Pair<UUID, V>? = getDescSequenceOfDocs(forKeys).firstOrNull()

        /**
         * Finds document ID matching lowest key. If there are multiple documents matching the lowest key, one is picked arbitrarily.
         * @param forKeys only consider documents mapped to these keys.
         */
        public fun findLastID(forKeys: For<K>): UUID? = getDescSequenceOfIDs(forKeys).firstOrNull()

        /**
         * Checks whether the index contains *value* document with given [id].
         *
         * Highly inefficient operation! O(n)
         */
        public fun containsValueDocument(id: UUID): Boolean = getSequenceOfIDs(For.all()).contains(id)

        /**
         * Returns a paged data loader which loads keys of this index.
         */
        public fun keyLoader(): DataLoader<K> = KeyLoader(indexStore)
    }

    override fun <V : Any> find(id: UUID, clazz: KClass<V>): V? = delegate.find(id, clazz)

    override fun exists(id: UUID): Boolean = delegate.exists(id)

    override fun delete(id: UUID) {
        indexUpdaterGlobalLock.withLock {
            val document: Any = find(id, Any::class) ?: return
            delegate.delete(id)
            indexMap.values.forEach { it.onDeleted(id, document) }
        }
    }

    override fun update(id: UUID, document: Any) {
        indexUpdaterGlobalLock.withLock {
            val oldDoc: Any = get(id, Any::class)
            delegate.update(id, document)
            indexMap.values.forEach { it.onUpdate(id, document, oldDoc) }
        }
    }

    override fun <K : Comparable<K>, V> openUtilityMap(name: String, keyType: DataType?, valueType: DataType?): UtilityMap<K, V> = delegate.openUtilityMap(name, keyType, valueType)

    override fun close() {
        delegate.close()
    }

    /**
     * Returns a particular index with given [name], failing if no such index was provided to the constructor.
     */
    @Suppress("UNCHECKED_CAST")
    public fun <K, D: Any, V: Any> getIndex(name: String): IndexImpl<K, D, V> where K: Comparable<K>, K: Serializable {
        return indexMap[name] as IndexImpl<K, D, V>? ?: throw IllegalStateException("Unknown index name $name, available indices: ${indexMap.keys}")
    }

    @Suppress("UNCHECKED_CAST")
    public fun <K, D: Any, V: Any> getIndex(index: IndexDef<K, D, V>): IndexImpl<K, D, V> where K: Comparable<K>, K: Serializable {
        return indexMap[index.name] as IndexImpl<K, D, V>? ?: throw IllegalStateException("Unknown index $index, available indices: ${indexMap.keys}")
    }

    override val typeToClassResolver: TypeToClassResolver
        get() = delegate.typeToClassResolver

    override fun findSerialized(id: UUID): SerializedDocument? = delegate.findSerialized(id)

    override fun getSequenceOfIDs(): Sequence<UUID> = delegate.getSequenceOfIDs()

    // @todo implement unique index
}

/**
 * The key matching criteria. There are four criteria:
 * * [Key] - a single key
 * * [All] - all possible keys (the *universal set* of all possible keys)
 * * [KeySet] - a set of keys
 * * [KeyRange] - a range of keys
 * @param K the type of the key.
 */
public sealed class For<K: Comparable<K>> {
    public abstract val isEmpty: Boolean

    public data class KeyRange<K: Comparable<K>>(val range: ClosedRange<K>) : For<K>() {
        override val isEmpty: Boolean
            get() = range.isEmpty()
    }

    public data class KeySet<K: Comparable<K>>(val set: Set<K>) : For<K>() {
        public constructor(vararg set: K) : this(set.toSet())
        override val isEmpty: Boolean
            get() = set.isEmpty()
    }

    public data class Key<K: Comparable<K>>(val key: K) : For<K>() {
        override val isEmpty: Boolean
            get() = false
    }

    public companion object {
        public fun <K: Comparable<K>> all(): For<K> = All()
    }

    public class All<K: Comparable<K>>: For<K>() {
        override val isEmpty: Boolean
            get() = false
    }
}

public class KeyLoader<K : Comparable<K>>(private val map: UtilityMap<K, *>) : DataLoader<K> {
    override fun fetch(filter: Filter<K>?, sortBy: List<SortClause>, range: LongRange): List<K> {
        require(filter == null) { "filtering is unsupported: $filter" }
        require(sortBy.isEmpty()) { "sorting is unsupported: $sortBy" }
        require(range.isEmpty() || range.first >= 0) { "range $range contains negative indices" }
        return map.keyPage(range.intRange)
    }

    override fun getCount(filter: Filter<K>?): Long {
        require(filter == null) { "filtering is unsupported: $filter" }
        return map.keyCount().toLong()
    }
}
